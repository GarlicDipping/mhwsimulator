﻿using System.Windows.Forms;

namespace MHWSimulator
{
	partial class FormMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.WrapperTableLayout = new System.Windows.Forms.TableLayoutPanel();
			this.UpperTableLayout = new System.Windows.Forms.TableLayoutPanel();
			this.AllSkillListTableLayout = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.searchTextBox = new System.Windows.Forms.TextBox();
			this.allSkillListBox = new System.Windows.Forms.ListBox();
			this.addSkillMinLvTable = new System.Windows.Forms.TableLayoutPanel();
			this.maxLvLabel = new System.Windows.Forms.Label();
			this.minSkillLvToAddNumeric = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
			this.addSkillBtn = new System.Windows.Forms.Button();
			this.removeSkillBtn = new System.Windows.Forms.Button();
			this.selectedSkillTableLayout = new System.Windows.Forms.TableLayoutPanel();
			this.RunButton = new System.Windows.Forms.Button();
			this.selectedSkillListBox = new System.Windows.Forms.ListBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.minSkillLvToRemoveNumeric = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.selectedSkillLabel = new System.Windows.Forms.Label();
			this.BottomTableLayout = new System.Windows.Forms.TableLayoutPanel();
			this.selectedResultSetLabel = new System.Windows.Forms.Label();
			this.ResultListTableLayout = new System.Windows.Forms.TableLayoutPanel();
			this.resultListView = new System.Windows.Forms.ListView();
			this.Head = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Body = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Arm = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Waist = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Leg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Charm = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.resultLabel = new System.Windows.Forms.Label();
			this.WrapperTableLayout.SuspendLayout();
			this.UpperTableLayout.SuspendLayout();
			this.AllSkillListTableLayout.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.addSkillMinLvTable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.minSkillLvToAddNumeric)).BeginInit();
			this.tableLayoutPanel5.SuspendLayout();
			this.selectedSkillTableLayout.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.minSkillLvToRemoveNumeric)).BeginInit();
			this.BottomTableLayout.SuspendLayout();
			this.ResultListTableLayout.SuspendLayout();
			this.SuspendLayout();
			// 
			// WrapperTableLayout
			// 
			this.WrapperTableLayout.ColumnCount = 1;
			this.WrapperTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.WrapperTableLayout.Controls.Add(this.UpperTableLayout, 0, 0);
			this.WrapperTableLayout.Controls.Add(this.BottomTableLayout, 0, 1);
			this.WrapperTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.WrapperTableLayout.Location = new System.Drawing.Point(0, 0);
			this.WrapperTableLayout.Name = "WrapperTableLayout";
			this.WrapperTableLayout.RowCount = 2;
			this.WrapperTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.WrapperTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.WrapperTableLayout.Size = new System.Drawing.Size(923, 702);
			this.WrapperTableLayout.TabIndex = 0;
			// 
			// UpperTableLayout
			// 
			this.UpperTableLayout.AutoSize = true;
			this.UpperTableLayout.ColumnCount = 3;
			this.UpperTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.UpperTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.UpperTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.UpperTableLayout.Controls.Add(this.AllSkillListTableLayout, 0, 0);
			this.UpperTableLayout.Controls.Add(this.tableLayoutPanel5, 1, 0);
			this.UpperTableLayout.Controls.Add(this.selectedSkillTableLayout, 2, 0);
			this.UpperTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.UpperTableLayout.Location = new System.Drawing.Point(3, 3);
			this.UpperTableLayout.Name = "UpperTableLayout";
			this.UpperTableLayout.RowCount = 1;
			this.UpperTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.UpperTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 274F));
			this.UpperTableLayout.Size = new System.Drawing.Size(917, 274);
			this.UpperTableLayout.TabIndex = 1;
			// 
			// AllSkillListTableLayout
			// 
			this.AllSkillListTableLayout.ColumnCount = 1;
			this.AllSkillListTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.AllSkillListTableLayout.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.AllSkillListTableLayout.Controls.Add(this.allSkillListBox, 0, 1);
			this.AllSkillListTableLayout.Controls.Add(this.addSkillMinLvTable, 0, 2);
			this.AllSkillListTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AllSkillListTableLayout.Location = new System.Drawing.Point(3, 3);
			this.AllSkillListTableLayout.Name = "AllSkillListTableLayout";
			this.AllSkillListTableLayout.RowCount = 3;
			this.AllSkillListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.AllSkillListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.AllSkillListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.AllSkillListTableLayout.Size = new System.Drawing.Size(392, 268);
			this.AllSkillListTableLayout.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.searchTextBox, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(386, 24);
			this.tableLayoutPanel2.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 24);
			this.label3.TabIndex = 12;
			this.label3.Text = "검색";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// searchTextBox
			// 
			this.searchTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.searchTextBox.Location = new System.Drawing.Point(53, 3);
			this.searchTextBox.Name = "searchTextBox";
			this.searchTextBox.Size = new System.Drawing.Size(330, 21);
			this.searchTextBox.TabIndex = 13;
			this.searchTextBox.TextChanged += new System.EventHandler(this.searchTextBox_TextChanged);
			// 
			// allSkillListBox
			// 
			this.allSkillListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.allSkillListBox.FormattingEnabled = true;
			this.allSkillListBox.ItemHeight = 12;
			this.allSkillListBox.Location = new System.Drawing.Point(3, 33);
			this.allSkillListBox.Name = "allSkillListBox";
			this.allSkillListBox.Size = new System.Drawing.Size(386, 202);
			this.allSkillListBox.TabIndex = 2;
			this.allSkillListBox.SelectedIndexChanged += new System.EventHandler(this.allSkillList_SelectedIndexChanged);
			// 
			// addSkillMinLvTable
			// 
			this.addSkillMinLvTable.ColumnCount = 3;
			this.addSkillMinLvTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
			this.addSkillMinLvTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.addSkillMinLvTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.addSkillMinLvTable.Controls.Add(this.maxLvLabel, 0, 0);
			this.addSkillMinLvTable.Controls.Add(this.minSkillLvToAddNumeric, 0, 0);
			this.addSkillMinLvTable.Controls.Add(this.label1, 0, 0);
			this.addSkillMinLvTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addSkillMinLvTable.Location = new System.Drawing.Point(3, 241);
			this.addSkillMinLvTable.Name = "addSkillMinLvTable";
			this.addSkillMinLvTable.RowCount = 1;
			this.addSkillMinLvTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.addSkillMinLvTable.Size = new System.Drawing.Size(386, 24);
			this.addSkillMinLvTable.TabIndex = 3;
			// 
			// maxLvLabel
			// 
			this.maxLvLabel.AutoSize = true;
			this.maxLvLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.maxLvLabel.Location = new System.Drawing.Point(289, 0);
			this.maxLvLabel.Name = "maxLvLabel";
			this.maxLvLabel.Size = new System.Drawing.Size(94, 25);
			this.maxLvLabel.TabIndex = 22;
			this.maxLvLabel.Text = "최대 레벨 : 5";
			this.maxLvLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// minSkillLvToAddNumeric
			// 
			this.minSkillLvToAddNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
			this.minSkillLvToAddNumeric.Location = new System.Drawing.Point(73, 3);
			this.minSkillLvToAddNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.minSkillLvToAddNumeric.Name = "minSkillLvToAddNumeric";
			this.minSkillLvToAddNumeric.Size = new System.Drawing.Size(210, 21);
			this.minSkillLvToAddNumeric.TabIndex = 7;
			this.minSkillLvToAddNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.minSkillLvToAddNumeric.ValueChanged += new System.EventHandler(this.minSkillLvToAddNumeric_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 25);
			this.label1.TabIndex = 6;
			this.label1.Text = "최소 레벨";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel5
			// 
			this.tableLayoutPanel5.ColumnCount = 1;
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel5.Controls.Add(this.addSkillBtn, 0, 1);
			this.tableLayoutPanel5.Controls.Add(this.removeSkillBtn, 0, 2);
			this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel5.Location = new System.Drawing.Point(401, 3);
			this.tableLayoutPanel5.Name = "tableLayoutPanel5";
			this.tableLayoutPanel5.RowCount = 4;
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.Size = new System.Drawing.Size(114, 268);
			this.tableLayoutPanel5.TabIndex = 2;
			// 
			// addSkillBtn
			// 
			this.addSkillBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.addSkillBtn.Location = new System.Drawing.Point(11, 98);
			this.addSkillBtn.Name = "addSkillBtn";
			this.addSkillBtn.Size = new System.Drawing.Size(92, 21);
			this.addSkillBtn.TabIndex = 2;
			this.addSkillBtn.Text = "스킬 추가 ▶";
			this.addSkillBtn.UseVisualStyleBackColor = true;
			this.addSkillBtn.Click += new System.EventHandler(this.addSkillBtn_Click);
			// 
			// removeSkillBtn
			// 
			this.removeSkillBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.removeSkillBtn.Location = new System.Drawing.Point(11, 148);
			this.removeSkillBtn.Name = "removeSkillBtn";
			this.removeSkillBtn.Size = new System.Drawing.Size(92, 21);
			this.removeSkillBtn.TabIndex = 4;
			this.removeSkillBtn.Text = "◀스킬 삭제";
			this.removeSkillBtn.UseVisualStyleBackColor = true;
			this.removeSkillBtn.Click += new System.EventHandler(this.removeSkillBtn_Click);
			// 
			// selectedSkillTableLayout
			// 
			this.selectedSkillTableLayout.ColumnCount = 1;
			this.selectedSkillTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.selectedSkillTableLayout.Controls.Add(this.RunButton, 0, 3);
			this.selectedSkillTableLayout.Controls.Add(this.selectedSkillListBox, 0, 0);
			this.selectedSkillTableLayout.Controls.Add(this.tableLayoutPanel3, 0, 1);
			this.selectedSkillTableLayout.Controls.Add(this.selectedSkillLabel, 0, 2);
			this.selectedSkillTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.selectedSkillTableLayout.Location = new System.Drawing.Point(521, 3);
			this.selectedSkillTableLayout.Name = "selectedSkillTableLayout";
			this.selectedSkillTableLayout.RowCount = 4;
			this.selectedSkillTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.selectedSkillTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.selectedSkillTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.selectedSkillTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.selectedSkillTableLayout.Size = new System.Drawing.Size(393, 268);
			this.selectedSkillTableLayout.TabIndex = 3;
			// 
			// RunButton
			// 
			this.RunButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RunButton.Location = new System.Drawing.Point(3, 221);
			this.RunButton.Name = "RunButton";
			this.RunButton.Size = new System.Drawing.Size(387, 44);
			this.RunButton.TabIndex = 18;
			this.RunButton.Text = "검색하기";
			this.RunButton.UseVisualStyleBackColor = true;
			this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
			// 
			// selectedSkillListBox
			// 
			this.selectedSkillListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.selectedSkillListBox.FormattingEnabled = true;
			this.selectedSkillListBox.ItemHeight = 12;
			this.selectedSkillListBox.Location = new System.Drawing.Point(3, 3);
			this.selectedSkillListBox.Name = "selectedSkillListBox";
			this.selectedSkillListBox.Size = new System.Drawing.Size(387, 152);
			this.selectedSkillListBox.TabIndex = 3;
			this.selectedSkillListBox.SelectedIndexChanged += new System.EventHandler(this.resultListView_SelectedIndexChanged);
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.minSkillLvToRemoveNumeric, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 161);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 1;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(387, 24);
			this.tableLayoutPanel3.TabIndex = 4;
			// 
			// minSkillLvToRemoveNumeric
			// 
			this.minSkillLvToRemoveNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
			this.minSkillLvToRemoveNumeric.Location = new System.Drawing.Point(73, 3);
			this.minSkillLvToRemoveNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.minSkillLvToRemoveNumeric.Name = "minSkillLvToRemoveNumeric";
			this.minSkillLvToRemoveNumeric.Size = new System.Drawing.Size(311, 21);
			this.minSkillLvToRemoveNumeric.TabIndex = 15;
			this.minSkillLvToRemoveNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.minSkillLvToRemoveNumeric.ValueChanged += new System.EventHandler(this.minSkillLvToRemoveNumeric_ValueChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Location = new System.Drawing.Point(3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 25);
			this.label5.TabIndex = 14;
			this.label5.Text = "최소 레벨";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// selectedSkillLabel
			// 
			this.selectedSkillLabel.AutoSize = true;
			this.selectedSkillLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.selectedSkillLabel.Location = new System.Drawing.Point(3, 188);
			this.selectedSkillLabel.Name = "selectedSkillLabel";
			this.selectedSkillLabel.Size = new System.Drawing.Size(387, 30);
			this.selectedSkillLabel.TabIndex = 19;
			this.selectedSkillLabel.Text = "선택한 스킬 :";
			this.selectedSkillLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BottomTableLayout
			// 
			this.BottomTableLayout.ColumnCount = 2;
			this.BottomTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
			this.BottomTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.BottomTableLayout.Controls.Add(this.selectedResultSetLabel, 1, 0);
			this.BottomTableLayout.Controls.Add(this.ResultListTableLayout, 0, 0);
			this.BottomTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BottomTableLayout.Location = new System.Drawing.Point(3, 283);
			this.BottomTableLayout.Name = "BottomTableLayout";
			this.BottomTableLayout.RowCount = 1;
			this.BottomTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BottomTableLayout.Size = new System.Drawing.Size(917, 416);
			this.BottomTableLayout.TabIndex = 2;
			// 
			// selectedResultSetLabel
			// 
			this.selectedResultSetLabel.AutoSize = true;
			this.selectedResultSetLabel.Dock = System.Windows.Forms.DockStyle.Left;
			this.selectedResultSetLabel.Location = new System.Drawing.Point(690, 0);
			this.selectedResultSetLabel.Name = "selectedResultSetLabel";
			this.selectedResultSetLabel.Size = new System.Drawing.Size(57, 416);
			this.selectedResultSetLabel.TabIndex = 20;
			this.selectedResultSetLabel.Text = "스킬 총합";
			this.selectedResultSetLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ResultListTableLayout
			// 
			this.ResultListTableLayout.ColumnCount = 1;
			this.ResultListTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.ResultListTableLayout.Controls.Add(this.resultListView, 0, 2);
			this.ResultListTableLayout.Controls.Add(this.resultLabel, 0, 0);
			this.ResultListTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ResultListTableLayout.Location = new System.Drawing.Point(3, 3);
			this.ResultListTableLayout.Name = "ResultListTableLayout";
			this.ResultListTableLayout.RowCount = 3;
			this.ResultListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.ResultListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.ResultListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.ResultListTableLayout.Size = new System.Drawing.Size(681, 410);
			this.ResultListTableLayout.TabIndex = 0;
			// 
			// resultListView
			// 
			this.resultListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Head,
            this.Body,
            this.Arm,
            this.Waist,
            this.Leg,
            this.Charm});
			this.resultListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.resultListView.FullRowSelect = true;
			this.resultListView.Location = new System.Drawing.Point(3, 63);
			this.resultListView.MultiSelect = false;
			this.resultListView.Name = "resultListView";
			this.resultListView.Size = new System.Drawing.Size(675, 344);
			this.resultListView.TabIndex = 21;
			this.resultListView.UseCompatibleStateImageBehavior = false;
			this.resultListView.View = System.Windows.Forms.View.Details;
			this.resultListView.SelectedIndexChanged += new System.EventHandler(this.resultListView_SelectedIndexChanged);
			this.resultListView.Resize += new System.EventHandler(this.resultListView_SizeChanged);
			// 
			// Head
			// 
			this.Head.Tag = "1";
			this.Head.Text = "머리";
			this.Head.Width = 110;
			// 
			// Body
			// 
			this.Body.Tag = "1";
			this.Body.Text = "몸통";
			this.Body.Width = 110;
			// 
			// Arm
			// 
			this.Arm.Tag = "1";
			this.Arm.Text = "팔";
			this.Arm.Width = 110;
			// 
			// Waist
			// 
			this.Waist.Tag = "1";
			this.Waist.Text = "허리";
			this.Waist.Width = 110;
			// 
			// Leg
			// 
			this.Leg.Tag = "1";
			this.Leg.Text = "다리";
			this.Leg.Width = 110;
			// 
			// Charm
			// 
			this.Charm.Tag = "1";
			this.Charm.Text = "호석";
			this.Charm.Width = 110;
			// 
			// resultLabel
			// 
			this.resultLabel.AutoSize = true;
			this.resultLabel.Dock = System.Windows.Forms.DockStyle.Left;
			this.resultLabel.Location = new System.Drawing.Point(3, 0);
			this.resultLabel.Name = "resultLabel";
			this.resultLabel.Size = new System.Drawing.Size(29, 30);
			this.resultLabel.TabIndex = 10;
			this.resultLabel.Text = "결과";
			this.resultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(923, 702);
			this.Controls.Add(this.WrapperTableLayout);
			this.Name = "FormMain";
			this.Text = "FormMain";
			this.WrapperTableLayout.ResumeLayout(false);
			this.WrapperTableLayout.PerformLayout();
			this.UpperTableLayout.ResumeLayout(false);
			this.AllSkillListTableLayout.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.addSkillMinLvTable.ResumeLayout(false);
			this.addSkillMinLvTable.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.minSkillLvToAddNumeric)).EndInit();
			this.tableLayoutPanel5.ResumeLayout(false);
			this.selectedSkillTableLayout.ResumeLayout(false);
			this.selectedSkillTableLayout.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.minSkillLvToRemoveNumeric)).EndInit();
			this.BottomTableLayout.ResumeLayout(false);
			this.BottomTableLayout.PerformLayout();
			this.ResultListTableLayout.ResumeLayout(false);
			this.ResultListTableLayout.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel WrapperTableLayout;
		private System.Windows.Forms.TableLayoutPanel UpperTableLayout;
		private System.Windows.Forms.TableLayoutPanel AllSkillListTableLayout;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox searchTextBox;
		private System.Windows.Forms.ListBox allSkillListBox;
		private System.Windows.Forms.TableLayoutPanel addSkillMinLvTable;
		private System.Windows.Forms.Label maxLvLabel;
		private System.Windows.Forms.NumericUpDown minSkillLvToAddNumeric;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.Button addSkillBtn;
		private System.Windows.Forms.Button removeSkillBtn;
		private System.Windows.Forms.TableLayoutPanel selectedSkillTableLayout;
		private System.Windows.Forms.Button RunButton;
		private System.Windows.Forms.ListBox selectedSkillListBox;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.NumericUpDown minSkillLvToRemoveNumeric;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label selectedSkillLabel;
		private System.Windows.Forms.TableLayoutPanel BottomTableLayout;
		private System.Windows.Forms.TableLayoutPanel ResultListTableLayout;
		private System.Windows.Forms.Label resultLabel;
		private System.Windows.Forms.Label selectedResultSetLabel;
		private System.Windows.Forms.ListView resultListView;
		private System.Windows.Forms.ColumnHeader Head;
		private System.Windows.Forms.ColumnHeader Body;
		private System.Windows.Forms.ColumnHeader Arm;
		private System.Windows.Forms.ColumnHeader Waist;
		private System.Windows.Forms.ColumnHeader Leg;
		private System.Windows.Forms.ColumnHeader Charm;
	}
}