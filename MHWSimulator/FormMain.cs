﻿using MHWSimulator.Scripts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MHWSimulator
{
	public partial class FormMain : Form
	{
		DBCsvFileReader db_reader;
		public FormMain()
		{
			InitializeComponent();
			InitializeQueryWorker();
			FillSkillSelectList();
		}

		void InitializeQueryWorker()
		{
			this.queryWorker = new System.ComponentModel.BackgroundWorker();
			queryWorker.WorkerSupportsCancellation = true;
			queryWorker.DoWork += new DoWorkEventHandler(queryWorker_DoWork);
			queryWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(queryWorker_RunWorkerCompleted);
		}

		BindingSource allSkillListBS = new BindingSource();
		BindingSource selectedSkillListBS = new BindingSource();
		List<QueriedSetsResult> resultSetsList = new List<QueriedSetsResult>();

		List<SelectedSkill> selectedSkillsData = new List<SelectedSkill>();
		SelectedSkill skillToAdd;
		SelectedSkill skillToRemove
		{
			get
			{
				if (selectedSkillListBox.SelectedValue != null)
				{
					return (SelectedSkill)selectedSkillListBox.SelectedValue;
				}
				return null;
			}
		}

		private void FillSkillSelectList()
		{
			db_reader = new DBCsvFileReader();
			//모든 스킬 리스트창 이닛
			allSkillListBS.DataSource = db_reader.skillNames.ToList();
			allSkillListBox.DataSource = allSkillListBS;

			//선택한 스킬 리스트창 이닛
			selectedSkillListBS.DataSource = selectedSkillsData;
			selectedSkillListBox.DataSource = selectedSkillListBS;
			selectedSkillListBox.DisplayMember = "displayName";
		}

		void RefreshNumerics()
		{
			RefreshAddNumerics();
			RefreshRemoveNumerics();
			RefreshSelectedSkillLabel();
		}

		void RefreshAddNumerics()
		{
			if (skillToAdd != null)
			{
				int max_skill_lv = db_reader.skillMaxLevelDict[skillToAdd.skillName];
				minSkillLvToAddNumeric.Minimum = 1;
				minSkillLvToAddNumeric.Maximum = max_skill_lv;
				minSkillLvToAddNumeric.Value = skillToAdd.min_lv;

				if (minSkillLvToAddNumeric.Value > skillToAdd.max_lv)
				{
					minSkillLvToAddNumeric.Value = skillToAdd.max_lv;
				}
			}
			else
			{
				minSkillLvToAddNumeric.Minimum = 1;
				minSkillLvToAddNumeric.Maximum = 1;
				minSkillLvToAddNumeric.Value = 1;
			}
		}

		void RefreshRemoveNumerics()
		{
			if (skillToRemove != null)
			{
				int max_skill_lv = db_reader.skillMaxLevelDict[skillToRemove.skillName];
				minSkillLvToRemoveNumeric.Minimum = 1;
				minSkillLvToRemoveNumeric.Maximum = max_skill_lv;
				minSkillLvToRemoveNumeric.Value = skillToRemove.min_lv;
				if (minSkillLvToRemoveNumeric.Value > skillToRemove.max_lv)
				{
					minSkillLvToRemoveNumeric.Value = skillToRemove.max_lv;
				}
			}
			else
			{
				minSkillLvToRemoveNumeric.Minimum = 1;
				minSkillLvToRemoveNumeric.Maximum = 1;
				minSkillLvToRemoveNumeric.Value = 1;
			}
		}

		void RefreshSelectedSkillLabel()
		{
			if (skillToRemove != null)
			{
				selectedSkillLabel.Text = skillToRemove.skillName;
			}
			else
			{
				selectedSkillLabel.Text = string.Empty;
			}
		}

		private void searchTextBox_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(searchTextBox.Text))
			{
				allSkillListBS.DataSource = db_reader.skillNames.ToList();
			}
			else
			{
				allSkillListBS.DataSource = db_reader.QuerySkillNameSkills(searchTextBox.Text);
			}
			allSkillListBS.ResetBindings(false);
			allSkillListBox.ClearSelected();
			skillToAdd = null;
		}

		private void allSkillList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (allSkillListBox.SelectedValue != null)
			{
				string skillToAddString = (string)allSkillListBox.SelectedValue;
				int max_skill_lv = db_reader.skillMaxLevelDict[skillToAddString];

				skillToAdd = new SelectedSkill(skillToAddString, 1, max_skill_lv);
				maxLvLabel.Text = "최대 레벨 : " + max_skill_lv;
			}
			RefreshNumerics();
		}

		private void selectedSkillList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (skillToRemove != null)
			{
				Console.WriteLine("SkillToRemove : " + skillToRemove.skillName);
				RefreshNumerics();
				minSkillLvToRemoveNumeric.Value = skillToRemove.min_lv;
			}
		}

		private void minSkillLvToAddNumeric_ValueChanged(object sender, EventArgs e)
		{
			if (skillToAdd != null)
			{
				if (skillToAdd.min_lv != minSkillLvToAddNumeric.Value)
				{
					skillToAdd.min_lv = (int)minSkillLvToAddNumeric.Value;
					RefreshNumerics();
				}
			}
		}

		private void addSkillBtn_Click(object sender, EventArgs e)
		{
			if (skillToAdd != null)
			{
				bool skillAlreadyExists = (from skill in selectedSkillsData
										   where skill.skillName == skillToAdd.skillName
										   select skill).Count() != 0;
				if (skillAlreadyExists == false)
				{
					string skillName = (string)allSkillListBox.SelectedItem;
					int min_lv = (int)minSkillLvToAddNumeric.Value;
					int max_lv = db_reader.skillMaxLevelDict[skillName];
					SelectedSkill selSkill = new SelectedSkill(skillName, min_lv, max_lv);
					selectedSkillsData.Add(selSkill);
					selectedSkillListBS.ResetBindings(false);

					selectedSkillListBox.SelectedIndex = selectedSkillsData.IndexOf(selSkill);

					RefreshNumerics();
				}
			}
		}

		private void removeSkillBtn_Click(object sender, EventArgs e)
		{
			if (skillToRemove != null)
			{
				selectedSkillsData.Remove(skillToRemove);
				selectedSkillListBS.ResetBindings(false);
				selectedSkillListBox.ClearSelected();
				RefreshNumerics();
			}
		}

		private void refreshLvRangeButton_Click(object sender, EventArgs e)
		{
			if (skillToRemove != null)
			{
				skillToRemove.min_lv = (int)minSkillLvToRemoveNumeric.Value;
				selectedSkillListBS.ResetBindings(false);
			}
		}

		private BackgroundWorker queryWorker;
		private void RunButton_Click(object sender, EventArgs e)
		{
			if (selectedSkillsData.Count == 0)
			{
				return;
			}
			resultSetsList.Clear();
			this.queryWorker.RunWorkerAsync();
			RunButton.Text = "검색중입니다...";
		}

		private void queryWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			var sets = MHWQueryHelper.QueryEquipment(db_reader, selectedSkillsData);
			e.Result = sets;
		}

		private void queryWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			RunButton.Text = "검색하기";
			resultSetsList.Clear();
			var sets = (List<Dictionary<EquipmentPart, EquipmentInfo>>)e.Result;
			if (sets != null)
			{
				resultListView.BeginUpdate();
				resultListView.Items.Clear();
				foreach (var set in sets)
				{
					resultSetsList.Add(new QueriedSetsResult(set));
					ListViewItem item = null;
					foreach (var part in MHWQueryHelper.equipmentIterateOrder)
					{
						if (set.ContainsKey(part))
						{
							string eqName = set[part].equipmentName;
							if (item == null)
							{
								item = new ListViewItem(eqName);
							}
							else
							{
								item.SubItems.Add(eqName);
							}
						}
						else
						{
							if (item == null)
							{
								item = new ListViewItem();
							}
							else
							{
								item.SubItems.Add(string.Empty);
							}
						}
					}
					resultListView.Items.Add(item);
				}
				resultListView.EndUpdate();
				resultLabel.Text = "결과 : [" + resultSetsList.Count + "]";
			}
		}

		private void resultListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (resultListView.SelectedIndices != null)
			{
				if (resultListView.SelectedIndices.Count > 0)
				{
					int selectedIndex = resultListView.SelectedIndices[0];
					QueriedSetsResult result = resultSetsList[selectedIndex];
					selectedResultSetLabel.Text = "스킬 총합\n" + result.GetSkillInfoString();
				}
				//QueriedSetsResult result = (QueriedSetsResult)resultListBox.SelectedValue;
				//selectedResultSetLabel.Text = "스킬 총합\n" + result.GetSkillInfoString();
			}
		}

		private void minSkillLvToRemoveNumeric_ValueChanged(object sender, EventArgs e)
		{
			if (skillToRemove != null)
			{
				skillToRemove.min_lv = (int)minSkillLvToRemoveNumeric.Value;
				selectedSkillListBS.ResetBindings(false);
			}
		}

		private bool resultListViewResizing = false;
		private void resultListView_SizeChanged(object sender, EventArgs e)
		{
			// Don't allow overlapping of SizeChanged calls
			if (!resultListViewResizing)
			{
				// Set the resizing flag
				resultListViewResizing = true;

				ListView listView = sender as ListView;
				if (listView != null)
				{
					float totalColumnWidth = 0;

					// Get the sum of all column tags
					for (int i = 0; i < listView.Columns.Count; i++)
						totalColumnWidth += Convert.ToInt32(listView.Columns[i].Tag);

					// Calculate the percentage of space each column should 
					// occupy in reference to the other columns and then set the 
					// width of the column to that percentage of the visible space.
					for (int i = 0; i < listView.Columns.Count; i++)
					{
						float colPercentage = (Convert.ToInt32(listView.Columns[i].Tag) / totalColumnWidth);
						listView.Columns[i].Width = (int)(colPercentage * listView.ClientRectangle.Width);
					}
				}
			}

			// Clear the resizing flag
			resultListViewResizing = false;
		}
	}
}
