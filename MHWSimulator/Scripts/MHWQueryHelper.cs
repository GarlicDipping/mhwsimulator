﻿using MHWSimulator.Scripts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MHWSimulator
{
	public static class MHWQueryHelper
	{
		public static EquipmentPart[] equipmentIterateOrder =
		{
			EquipmentPart.Head,
			EquipmentPart.Body,
			EquipmentPart.Arm,
			EquipmentPart.Waist,
			EquipmentPart.Leg,
			EquipmentPart.Charm
		};
		
		public static List<Dictionary<EquipmentPart, EquipmentInfo>> QueryEquipment(DBCsvFileReader db, List<SelectedSkill> skillRequirements)
		{			
			Dictionary<string, List<Dictionary<EquipmentPart, EquipmentInfo>>> skillEquipmentDict =
				new Dictionary<string, List<Dictionary<EquipmentPart, EquipmentInfo>>>();			
			foreach (var requirement in skillRequirements)
			{
				var candidates = fetchInitialCandidates(db, requirement);
				skillEquipmentDict.Add(requirement.skillName, candidates);		
			}

			List<Dictionary<EquipmentPart, EquipmentInfo>> result
				= new List<Dictionary<EquipmentPart, EquipmentInfo>>();
			recursiveFitEquipment(skillRequirements, 0, skillEquipmentDict, result, new Dictionary<EquipmentPart, EquipmentInfo>());

			return result;
		}

		static void recursiveFitEquipment(List<SelectedSkill> requirements, int requirementIndex,
			Dictionary<string, List<Dictionary<EquipmentPart, EquipmentInfo>>> totalCandidates,
			List<Dictionary<EquipmentPart, EquipmentInfo>> result,			
			Dictionary<EquipmentPart, EquipmentInfo> currentSet)
		{
			if(requirementIndex > requirements.Count - 1)
			{
				//재귀호출 바닥 찍음. continue를 통해 실패 케이스는 처리되므로 success로 판단!
				var successSet = cloneEquipmentSet(currentSet);
				result.Add(successSet);
				return;
			}
			
			string targetSkill = requirements[requirementIndex].skillName;
			var currentCandidateSets = totalCandidates[targetSkill];
			for(int cand_idx = 0; cand_idx < currentCandidateSets.Count; cand_idx++)
			{
				var candidateSet = currentCandidateSets[cand_idx];
				if (TryAddCurrentSet(candidateSet, currentSet) == false)
				{
					//세트에 중복 파츠가 있음, 실패!
					continue;
				}

				//한 세트 모두 돌았음, 다음 depth가 존재하면 들어감
				recursiveFitEquipment(requirements, requirementIndex + 1, totalCandidates, result, currentSet);
				
				//currentSet에서 색인 마무리한 후보 셋은 삭제
				foreach (var key in candidateSet.Keys)
				{
					currentSet.Remove(key);
				}
			}
		}

		static bool TryAddCurrentSet(Dictionary<EquipmentPart, EquipmentInfo> candidateSet, 
			Dictionary<EquipmentPart, EquipmentInfo> currentSet)
		{
			List<EquipmentPart> keyList = candidateSet.Keys.ToList();
			List<EquipmentPart> duplicatedParts = new List<EquipmentPart>();
			for (int key_idx = 0; key_idx < keyList.Count; key_idx++)
			{
				EquipmentPart part = keyList[key_idx];
				if (currentSet.ContainsKey(part))
				{
					if(currentSet[part].isSameEquipment(candidateSet[part]) == false)
					{
						//이 세트는 중복 파츠가 존재하여 적용 불가.
						for (int reverseRemoveIdx = key_idx - 1; reverseRemoveIdx >= 0; reverseRemoveIdx--)
						{
							EquipmentPart partToRemove = keyList[reverseRemoveIdx];
							if (duplicatedParts.Contains(partToRemove) == false)
							{
								currentSet.Remove(partToRemove);
							}
						}
					}
					else
					{
						//중복 파츠지만 동일한 파츠면 OK
						duplicatedParts.Add(part);
					}

					return false;
				}
				else
				{
					if(currentSet.ContainsKey(part) == false)
					{
						currentSet.Add(part, candidateSet[part]);
					}
				}
			}
			return true;
		}

		static List<Dictionary<EquipmentPart, EquipmentInfo>> fetchInitialCandidates(DBCsvFileReader db, SelectedSkill target_requirement)
		{
			string target_skill = target_requirement.skillName;
			Dictionary<EquipmentPart, List<EquipmentInfo>> equipmentsWithTargetSkill =
				EquipmentsHavingTargetSkill(db, target_requirement);
			List<Dictionary<EquipmentPart, EquipmentInfo>> successSets = new List<Dictionary<EquipmentPart, EquipmentInfo>>();
			recursiveFitEquipments(target_skill, equipmentsWithTargetSkill, successSets, null, 0, target_requirement.min_lv);
			return successSets;
		}

		static void recursiveFitEquipments(string target_skill,
			Dictionary<EquipmentPart, List<EquipmentInfo>> equipmentsWithTargetSkill,
			List<Dictionary<EquipmentPart, EquipmentInfo>> result,
			Dictionary<EquipmentPart, EquipmentInfo> equipmentSet,
			int equipment_part_index, int target_skill_lv)
		{
			if (equipmentSet == null)
			{
				equipmentSet = new Dictionary<EquipmentPart, EquipmentInfo>();
			}

			int current_skill_lv = TotalSkillLv(target_skill, equipmentSet);
			if (current_skill_lv >= target_skill_lv)
			{
				Dictionary<EquipmentPart, EquipmentInfo> successSet = cloneEquipmentSet(equipmentSet);
				result.Add(successSet);
			}

			if (equipment_part_index > equipmentIterateOrder.Length - 1)
			{
				//if (current_skill_lv >= target_skill_lv)
				//{
				//	Dictionary<EquipmentPart, EquipmentInfo> successSet = cloneEquipmentSet(equipmentSet);
				//	result.Add(successSet);
				//}
				//return;
			}
			else
			{
				for(int part_index = equipment_part_index; part_index < equipmentIterateOrder.Length; part_index++)
				{
					EquipmentPart current_iter_part = equipmentIterateOrder[part_index];
					List<EquipmentInfo> equipmentList = equipmentsWithTargetSkill[current_iter_part];
					for (int equipment_index = 0; equipment_index < equipmentList.Count; equipment_index++)
					{
						equipmentSet.Add(current_iter_part, equipmentList[equipment_index]);
						recursiveFitEquipments(target_skill, equipmentsWithTargetSkill, result, equipmentSet,
							part_index + 1,
							target_skill_lv);
						equipmentSet.Remove(current_iter_part);
					}
				}
			}
		}

		static Dictionary<EquipmentPart, EquipmentInfo> cloneEquipmentSet(Dictionary<EquipmentPart, EquipmentInfo> original)
		{
			Dictionary<EquipmentPart, EquipmentInfo> successSet = new Dictionary<EquipmentPart, EquipmentInfo>();
			foreach (var part in original.Keys)
			{
				successSet.Add(part, original[part].Clone());
			}
			return successSet;
		}

		static int TotalSkillLv(string target_skill, Dictionary<EquipmentPart, EquipmentInfo> equipmentSet)
		{
			int result = 0;
			foreach(var equipment in equipmentSet.Values)
			{
				if (equipment.skillLvDict.ContainsKey(target_skill))
				{
					result += equipment.skillLvDict[target_skill];
				}
			}
			return result;
		}

		//static List<Dictionary<EquipmentPart, EquipmentInfo>> recursiveFitEquipments(
		//	Dictionary<EquipmentPart, EquipmentInfo> current_set,
		//	Dictionary<EquipmentPart, List<EquipmentInfo>> equipmentsWithTargetSkill, 
		//	string target_skill_name, int equipment_part_index, int equipment_index, 
		//	int current_skill_lv, int target_skill_lv)
		//{
		//	if(current_skill_lv >= target_skill_lv)
		//	{
				
		//	}

		//	if(equipment_part_index < equipmentIterateOrder.Length)
		//	{
		//		//아직 더 검색할 수 있을 여지가 존재
		//		List<EquipmentInfo> equipmentList = equipmentsWithTargetSkill[equipmentIterateOrder[equipment_part_index]];
		//		EquipmentInfo current_equipment = equipmentList[equipment_index];

		//		if (equipment_index < equipmentList.Count)
		//		{
		//			return recursiveFitEquipments(result, equipmentsWithTargetSkill, target_skill_name, equipment_part_index + 1, equipment_index + 1,
		//				current_skill_lv + current_equipment.skillLvDict[target_skill_name], target_skill_lv);
		//		}
		//		else
		//		{
		//			return result;
		//		}
		//	}
		//	else
		//	{
		//		return result;
		//	}
		//}

		static Dictionary<EquipmentPart, List<EquipmentInfo>> EquipmentsHavingTargetSkill(DBCsvFileReader db, SelectedSkill skillRequirement)
		{
			var equipmentCandidates = db.skillEquipmentDict[skillRequirement.skillName];
			Dictionary<EquipmentPart, List<EquipmentInfo>> sortedEquipments = new Dictionary<EquipmentPart, List<EquipmentInfo>>();
			foreach(var equipmentPart in equipmentIterateOrder)
			{
				sortedEquipments.Add(equipmentPart, new List<EquipmentInfo>());
			}
			foreach(var equipment in equipmentCandidates)
			{
				sortedEquipments[equipment.part].Add(equipment);
			}
			return sortedEquipments;
		}
	}
}
