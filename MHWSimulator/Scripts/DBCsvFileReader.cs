﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHWSimulator
{
	/// <summary>
	/// 장착부위
	/// </summary>
	public enum EquipmentPart
	{
		/// <summary>
		/// NONE일 경우 뭔가 에러가 있는 것
		/// </summary>
		ERROR,
		Head,
		Body,
		Arm,
		Waist,
		Leg,
		Charm,		
	}
	/// <summary>
	/// 슬롯 크기
	/// </summary>
	public enum SlotType : int
	{
		None = 0,
		One = 1,
		Two = 2,
		Three = 3
	}
	public enum ResistenceType
	{
		Fire,
		Water,
		Lightening,
		Ice,
		Dragon
	}

	public struct EquipmentInfo
	{
		public string equipmentName;
		/// <summary>
		/// 부위
		/// </summary>
		public EquipmentPart part;
		/// <summary>
		/// 방어력
		/// </summary>
		public int def;
		/// <summary>
		/// 슬롯정보
		/// </summary>
		public SlotType[] slots;
		/// <summary>
		/// 내성
		/// </summary>
		public Dictionary<ResistenceType, int> resValueDict;
		public Dictionary<string, int> skillLvDict;

		public EquipmentInfo Clone()
		{
			EquipmentInfo cloned = new EquipmentInfo();
			cloned.equipmentName = equipmentName;
			cloned.part = part;
			cloned.def = def;
			cloned.slots = new SlotType[slots.Length];
			for(int i = 0; i < slots.Length; i++)
			{
				cloned.slots[i] = slots[i];
			}
			cloned.resValueDict = new Dictionary<ResistenceType, int>();
			foreach(var resType in resValueDict.Keys)
			{
				cloned.resValueDict.Add(resType, resValueDict[resType]);
			}
			cloned.skillLvDict = new Dictionary<string, int>();
			foreach(var skill in skillLvDict.Keys)
			{
				cloned.skillLvDict[skill] = skillLvDict[skill];
			}
			return cloned;
		}

		public bool isSameEquipment(EquipmentInfo info)
		{
			return equipmentName.Equals(info.equipmentName);
		}
	}

	/// <summary>
	/// 장신주 정보
	/// </summary>
	public struct DecorationInfo
	{
		public string skill;
		public string itemName;
		public int slotLv;
		public DecorationInfo(string itemName, string skill, int slotLv)
		{
			this.itemName = itemName;
			this.skill = skill;
			this.slotLv = slotLv;
		}

		public static bool operator==(DecorationInfo a, DecorationInfo b)
		{
			if(a != null && b != null)
			{
				return a.Equals(b);
			}
			else if(a == null && b == null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator !=(DecorationInfo a, DecorationInfo b)
		{
			if (a != null && b != null)
			{
				return !a.Equals(b);
			}
			else if (a == null && b == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public override bool Equals(object obj)
		{
			if(obj != null && obj is DecorationInfo)
			{
				DecorationInfo compare_target = (DecorationInfo)obj;
				if(skill.Equals(compare_target.skill) &&
					itemName.Equals(compare_target.itemName) &&
					slotLv.Equals(compare_target.slotLv))
				{
					return true;
				}
			}
			return false;
		}

		public override int GetHashCode()
		{
			return skill.GetHashCode() ^ itemName.GetHashCode() ^ slotLv;
		}
	}
	public class DBCsvFileReader
	{
		/// <summary>
		/// [스킬명, 방어구 및 호석 장비 정보]
		/// </summary>
		public Dictionary<string, List<EquipmentInfo>> skillEquipmentDict;
		/// <summary>
		/// [스킬명, 장신주 정보]
		/// </summary>
		public Dictionary<string, List<DecorationInfo>> skillDecoDict;
		/// <summary>
		/// [스킬명, 맥스레벨]
		/// </summary>
		public Dictionary<string, int> skillMaxLevelDict;
		public Dictionary<EquipmentPart, List<EquipmentInfo>> partEquipmentDict;
		public List<string> skillNames
		{
			get
			{
				return skillMaxLevelDict.Keys.ToList();
			}
		}
		public DBCsvFileReader()
		{
			Read_SkillList();
			Read_Decorations();
			Read_Equipments();
		}

		public List<string> QuerySkillNameSkills(string name)
		{
			return (from skill in skillNames
					where skill.Contains(name)
					select skill).ToList();
		}

		void Read_SkillList()
		{
			skillMaxLevelDict = new Dictionary<string, int>();
			string[] csvText = Properties.Resources.몬헌_스킬리스트.Replace("\r\n","\n").Split('\n');

			for (int i = 1; i < csvText.Length; i++)
			{
				var row = csvText[i].Split(',');
				skillMaxLevelDict.Add(row[0], int.Parse(row[1]));
			}
		}

		void Read_Decorations()
		{
			skillDecoDict = new Dictionary<string, List<DecorationInfo>>();
			string[] csvText = Properties.Resources.몬헌_장식주.Replace("\r\n", "\n").Split('\n');
			for (int i = 1; i < csvText.Length; i++)
			{
				var row = csvText[i].Split(',');
				string itemName = row[0];
				int slotLv = int.Parse(row[1]);
				string skill = row[2];
				if (skillDecoDict.ContainsKey(skill) == false)
				{
					skillDecoDict.Add(skill, new List<DecorationInfo>());
				}
				skillDecoDict[skill].Add(new DecorationInfo(itemName, skill, slotLv));
			}
		}

		void Read_Equipments()
		{
			partEquipmentDict = new Dictionary<EquipmentPart, List<EquipmentInfo>>();
			foreach(var part in MHWQueryHelper.equipmentIterateOrder)
			{
				partEquipmentDict.Add(part, new List<EquipmentInfo>());
			}
			skillEquipmentDict = new Dictionary<string, List<EquipmentInfo>>();
			string[] csvText = Properties.Resources.몬헌_장비및호석.Replace("\r\n", "\n").Split('\n');
			for(int i = 1; i < csvText.Length; i++)
			{
				var row = csvText[i].Split(',');
				row = trimRows(row);
				EquipmentInfo equipmentInfo = new EquipmentInfo();
				equipmentInfo.equipmentName = row[0];
				equipmentInfo.part = partFromString(row[1]);
				if(string.IsNullOrEmpty(row[2]) == false)
				{
					equipmentInfo.def = int.Parse(row[2]);
				}				
				equipmentInfo.slots = slotsFromString(row[3]);				
				equipmentInfo.resValueDict = resValueDictFromString(row[4]);

				equipmentInfo.skillLvDict = new Dictionary<string, int>();
				if(string.IsNullOrEmpty(row[5]) == false)
				{
					equipmentInfo.skillLvDict.Add(row[5], int.Parse(row[8]));
					if (skillEquipmentDict.ContainsKey(row[5]) == false)
					{
						skillEquipmentDict.Add(row[5], new List<EquipmentInfo>());
					}
					skillEquipmentDict[row[5]].Add(equipmentInfo);
				}
				if (string.IsNullOrEmpty(row[6]) == false)
				{
					equipmentInfo.skillLvDict.Add(row[6], int.Parse(row[9]));
					if (skillEquipmentDict.ContainsKey(row[6]) == false)
					{
						skillEquipmentDict.Add(row[6], new List<EquipmentInfo>());
					}
					skillEquipmentDict[row[6]].Add(equipmentInfo);
				}
				if (string.IsNullOrEmpty(row[7]) == false)
				{
					equipmentInfo.skillLvDict.Add(row[7], int.Parse(row[10]));
					if (skillEquipmentDict.ContainsKey(row[7]) == false)
					{
						skillEquipmentDict.Add(row[7], new List<EquipmentInfo>());
					}
					skillEquipmentDict[row[7]].Add(equipmentInfo);
				}
				partEquipmentDict[equipmentInfo.part].Add(equipmentInfo);
			}
		}

		string[] trimRows(string[] rows)
		{
			for(int i = 0; i < rows.Length; i++)
			{
				rows[i] = rows[i].Trim();
			}
			return rows;
		}

		EquipmentPart partFromString(string partStr)
		{
			switch (partStr)
			{
				case "머리":
					return EquipmentPart.Head;
				case "몸통":
					return EquipmentPart.Body;
				case "팔":
					return EquipmentPart.Arm;
				case "허리":
					return EquipmentPart.Waist;
				case "다리":
					return EquipmentPart.Leg;
				case "호석":
					return EquipmentPart.Charm;
			}
			return EquipmentPart.ERROR;
		}

		SlotType[] slotsFromString(string slotStr)
		{
			SlotType[] slots = new SlotType[3];
			if (string.IsNullOrEmpty(slotStr))
			{
				slots[0] = SlotType.None;
				slots[1] = SlotType.None;
				slots[2] = SlotType.None;
			}
			else
			{
				string[] split = slotStr.Split(' ');
				for (int i = 0; i < 3; i++)
				{
					string slot_str = split[i];
					if (slot_str.Equals('-'))
					{
						slots[i] = SlotType.None;
					}
					else if (slot_str.Equals('1'))
					{
						slots[i] = SlotType.One;
					}
					else if (slot_str.Equals('2'))
					{
						slots[i] = SlotType.Two;
					}
					else if (slot_str.Equals('3'))
					{
						slots[i] = SlotType.Three;
					}
				}

			}
			return slots;
		}

		ResistenceType[] resTypeOrder =
		{
			ResistenceType.Fire,
			ResistenceType.Water,
			ResistenceType.Lightening,
			ResistenceType.Ice,
			ResistenceType.Dragon
		};

		Dictionary<ResistenceType, int> resValueDictFromString(string resValueStr)
		{
			Dictionary<ResistenceType, int> resDict = new Dictionary<ResistenceType, int>();
			if (string.IsNullOrEmpty(resValueStr))
			{
				foreach(var type in resTypeOrder)
				{
					resDict.Add(type, 0);
				}
			}
			else
			{
				string[] split = resValueStr.Split(':');
				//ex) 화:-3수:-2뇌:4빙:-2용:2
				//-> [화][-3수][-2뇌][4빙][-2용][2]
				for (int i = 1; i < split.Length; i++)
				{
					string value_str = string.Empty;
					if(i == split.Length - 1)
					{
						//마지막 스트링은 용 속성
						value_str = split[i];
					}
					else
					{
						value_str = split[i];
						value_str = value_str.Substring(0, value_str.Length - 1);
					}
					int result_val = int.Parse(value_str);
					resDict.Add(resTypeOrder[i - 1], result_val);
				}
			}
			return resDict;
		}

	}
}
