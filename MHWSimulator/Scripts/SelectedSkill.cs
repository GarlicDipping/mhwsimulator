﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHWSimulator.Scripts
{
	public class SelectedSkill
	{
		public SelectedSkill(string skillName, int min_lv, int max_lv)
		{
			this.skillName = skillName;
			this.min_lv = min_lv;
			this.max_lv = max_lv;
		}
		public string skillName;
		public int min_lv;
		public int max_lv { get; private set; }
		public string displayName
		{
			get
			{
				if (min_lv == max_lv)
				{
					return skillName + "\t[" + min_lv + "]";
				}
				else
				{
					return skillName + "\t[" + min_lv + " - " + max_lv + "]";
				}
			}
		}
	}

	public class QueriedSetsResult
	{
		public Dictionary<EquipmentPart, EquipmentInfo> set;
		public string SetResultDisplay
		{
			get
			{
				return LogString();
			}
		}
		public QueriedSetsResult(Dictionary<EquipmentPart, EquipmentInfo> set)
		{
			this.set = set;
		}

		public string GetSkillInfoString()
		{
			Dictionary<string, int> skills = new Dictionary<string, int>();
			foreach(var info in set.Values)
			{
				foreach(var skillName in info.skillLvDict.Keys)
				{
					if (skills.ContainsKey(skillName) == false)
					{
						skills.Add(skillName, 0);
					}
					skills[skillName] += info.skillLvDict[skillName];
				}
			}
			string result = string.Empty;
			foreach(string skillName in skills.Keys)
			{
				result += " : [" + skills[skillName] + "]      ";
				result += string.Format("{0,-10}", skillName) + "\n";
				//result += (skillName + " : ").PadRight(25) + ("[" + skills[skillName] + "]").PadLeft(25) + "\n";
			}
			return result.Substring(0, result.Length - 1);
		}

		string LogString()
		{
			string result = string.Empty;
			foreach(var key in set.Keys)
			{
				result += "[" + key.ToString() + "] : " + set[key].equipmentName;
			}
			return result;
		}
	}
}
